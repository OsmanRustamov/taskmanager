import sqlite3
from typing import List, Tuple

class TaskManager:
    def __init__(self, db_name: str) -> None:
        try:
            self.conn: sqlite3.Connection = sqlite3.connect(db_name)
            self.create_table()
        except Exception as e:
            raise RuntimeError(f"Ошибка при подключении к базе данных: {e}")

    def create_table(self) -> None:
        try:
            c: sqlite3.Cursor = self.conn.cursor()
            c.execute('''CREATE TABLE IF NOT EXISTS tasks
                         (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                         title TEXT NOT NULL UNIQUE, 
                         description TEXT, 
                         status TEXT)''')
            self.conn.commit()
        except Exception as e:
            raise RuntimeError(f"Ошибка при создании таблицы: {e}")

    def add_task(self, title: str, description: str, status: str = 'Выполняется') -> None:
        try:
            c: sqlite3.Cursor = self.conn.cursor()
            c.execute("INSERT INTO tasks (title, description, status) VALUES (?, ?, ?)", (title, description, status))
            self.conn.commit()
        except Exception as e:
            raise RuntimeError(f"Ошибка при добавлении задачи: {e}")

    def get_all_tasks(self) -> List[Tuple[int, str, str, str]]:
        try:
            c: sqlite3.Cursor = self.conn.cursor()
            c.execute("SELECT * FROM tasks")
            tasks: List[Tuple[int, str, str, str]] = c.fetchall()
            return tasks
        except Exception as e:
            raise RuntimeError(f"Ошибка при получении всех задач: {e}")

    def delete_task(self, title: str) -> None:
        try:
            c: sqlite3.Cursor = self.conn.cursor()
            c.execute("DELETE FROM tasks WHERE title=?", (title,))
            self.conn.commit()
        except Exception as e:
            raise RuntimeError(f"Ошибка при удалении задачи: {e}")

    def update_description(self, title: str, description: str) -> None:
        try:
            c: sqlite3.Cursor = self.conn.cursor()
            c.execute("UPDATE tasks SET description=? WHERE title=?", (description, title))
            self.conn.commit()
        except Exception as e:
            raise RuntimeError(f"Ошибка при обновлении описания: {e}")

    def update_status(self, title: str, status: str) -> None:
        try:
            c: sqlite3.Cursor = self.conn.cursor()
            c.execute("UPDATE tasks SET status=? WHERE title=?", (status, title))
            self.conn.commit()
        except Exception as e:
            raise RuntimeError(f"Ошибка при обновлении статуса: {e}")

    def close_connection(self) -> None:
        try:
            self.conn.close()
        except Exception as e:
            raise RuntimeError(f"Ошибка при закрытии соединения: {e}")
