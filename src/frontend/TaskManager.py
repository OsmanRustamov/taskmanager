from PyQt6.QtWidgets import QInputDialog, QWidget, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QPushButton, QListWidget, QMessageBox, QCheckBox, QMenu
from PyQt6.QtCore import QSize, Qt
from PyQt6.QtGui import QAction
from src.backend.TaskManagerBack import TaskManager


class TaskManagerApp(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Менеджер задач')
        self.setGeometry(100, 100, 400, 300)

        self.task_manager = TaskManager("tasks.db")

        self.title_label = QLabel("Заголовок:")
        self.title_input = QLineEdit()
        self.description_label = QLabel("Описание:")
        self.description_input = QLineEdit()
        self.add_button = QPushButton("Добавить задачу")
        self.task_list = QListWidget()

        self.init_ui()

    def init_ui(self):
        layout = QVBoxLayout()

        form_layout = QVBoxLayout()
        form_layout.addWidget(self.title_label)
        form_layout.addWidget(self.title_input)
        form_layout.addWidget(self.description_label)
        form_layout.addWidget(self.description_input)
        form_layout.addWidget(self.add_button)

        self.add_button.clicked.connect(self.add_task)

        layout.addLayout(form_layout)
        layout.addWidget(self.task_list)

        self.setLayout(layout)
        self.refresh_task_list()

    def add_task(self):
        title = self.title_input.text()
        description = self.description_input.text()
        if title not in self.task_manager.get_all_tasks():
            self.task_manager.add_task(title, description)

        self.refresh_task_list()
        self.title_color: {colors['dark']};input.clear()
        self.description_input.clear()

    def contextMenuEvent(self, event):
        context_menu = QMenu(self)

        delete_action = QAction("Удалить задачу", self)
        delete_action.triggered.connect(self.delete_selected_task)
        context_menu.addAction(delete_action)

        update_description_action = QAction("Обновить описание", self)
        update_description_action.triggered.connect(self.update_selected_description)
        context_menu.addAction(update_description_action)

        update_status_action = QAction("Обновить статус", self)
        update_status_action.triggered.connect(self.update_selected_status)
        context_menu.addAction(update_status_action)

        context_menu.exec(self.mapToGlobal(event.pos()))

    def delete_selected_task(self):
        title_of_selected_task = self.task_list.currentItem().text().split(" - ")[0]
        self.task_manager.delete_task(title_of_selected_task)
        self.refresh_task_list()

    def update_selected_description(self):
        selected_task = self.task_list.currentItem().text().split(" - ")[0]
        new_description, ok = QInputDialog.getText(self, 'Обновить описание', 'Введите новое описание:')
        if ok:
            self.task_manager.update_description(selected_task, new_description)
            self.refresh_task_list()

    def update_selected_status(self):
        selected_task = self.task_list.currentItem().text().split(" - ")[0]

        dialog = QMessageBox()
        dialog.setWindowTitle("Выберите новый статус")
        dialog.setText("Выберите новый статус:")
        dialog.addButton("Выполняется", QMessageBox.ButtonRole.ActionRole)
        dialog.addButton("Сделано", QMessageBox.ButtonRole.ActionRole)

        if dialog.exec() == 0:
            new_status = "Выполняется"
        else:
            new_status = "Сделано"

        self.task_manager.update_status(selected_task, new_status)
        self.refresh_task_list()

    def refresh_task_list(self):
        self.task_list.clear()
        tasks = self.task_manager.get_all_tasks()
        for task in tasks:
            self.task_list.addItem(f"{task[1]} - {task[2]}\n{task[3]}")

    def closeEvent(self, event):
        self.task_manager.close_connection()