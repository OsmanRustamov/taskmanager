import sys
from PyQt6.QtGui import QPalette, QColor
from PyQt6.QtWidgets import QApplication
from frontend.TaskManager import TaskManagerApp

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = TaskManagerApp()
    window.show()
    colors = {
        'light': '#EAC696',
        'medium': '#C8AE7D',
        'dark': '#765827',
        'darker': '#65451F'
    }

    # Установка палитры цветов
    palette = QPalette()
    palette.setColor(QPalette.ColorRole.Window, QColor(colors['light']))
    palette.setColor(QPalette.ColorRole.WindowText, QColor(colors['dark']))
    palette.setColor(QPalette.ColorRole.Base, QColor(colors['medium']))
    palette.setColor(QPalette.ColorRole.AlternateBase, QColor(colors['medium']))
    palette.setColor(QPalette.ColorRole.ToolTipBase, QColor(colors['medium']))
    palette.setColor(QPalette.ColorRole.ToolTipText, QColor(colors['dark']))
    palette.setColor(QPalette.ColorRole.Text, QColor('black'))
    palette.setColor(QPalette.ColorRole.Button, QColor(colors['medium']))
    palette.setColor(QPalette.ColorRole.ButtonText, QColor(colors['dark']))
    palette.setColor(QPalette.ColorRole.BrightText, QColor(colors['light']))
    palette.setColor(QPalette.ColorRole.Link, QColor(colors['darker']))
    palette.setColor(QPalette.ColorRole.Highlight, QColor(colors['darker']))
    palette.setColor(QPalette.ColorRole.HighlightedText, QColor(colors['light']))

    app.setPalette(palette)

    app.setStyleSheet(f"QPushButton {{ border: none; background-color: {colors['dark']}; color: {colors['light']}; border-radius: 10px; padding: 10px; min-width: 120px; font-weight: normal; }}"
                      f"QListWidget {{ border: none; border-radius: 10px; background-color: {colors['medium']}; color: {colors['dark']}; }}"
                      f"QLineEdit {{ border: none; border-radius: 10px; color: {colors['dark']}; }}"
                      f"QMenu {{ border: none; border-radius: 10px; }}")


    sys.exit(app.exec())


